import { login, mainElement } from './index.js';
import { renderHomePage } from './home.js';

const xhr = new XMLHttpRequest(),
  backendURL = 'https://server.harka.com/football-calendar';

xhr.onerror = () => serverErrorHandling();

export let sportData = 0;

const notifyUser = () => {
  mainElement.innerHTML = `
      <div class="card card--big">
        <h1>Server error! :(</h1>
        <p>The server can't be reached for some reason.</p>
        <p>Please refresh or try visiting at a later time.</p>
      </div>`;
};

const notifyDaniel = () => {
  const xhrDaniel = new XMLHttpRequest();
  xhrDaniel.open('POST', 'https://server.harka.com/message');
  xhrDaniel.setRequestHeader('Content-type', 'text/plain');
  xhrDaniel.send(`Football Calendar: Server down!`);
};

const serverErrorHandling = () => {
  notifyUser();
  notifyDaniel();
};

export const loadData = () => {
  xhr.open('GET', `${backendURL}/load`);
  xhr.onload = () => {
    sportData = JSON.parse(xhr.responseText).data;
  };
  xhr.send();
};

export const saveData = () => {
  xhr.open('PUT', `${backendURL}/save`);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = () => {};
  xhr.send(JSON.stringify({ data: sportData }));
};

export const resetData = () => {
  sportData = 0;
  xhr.open('PUT', `${backendURL}/reset`);
  xhr.onload = () => {
    sportData = JSON.parse(xhr.responseText).data;
  };
  xhr.send();
  sportData ? renderHomePage() : setTimeout(renderHomePage, 50);
};

export const initiateLogin = loginData => {
  xhr.open('POST', `${backendURL}/login`);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = () => {
    if (xhr.status === 200) {
      login();
    } else if (xhr.status === 401) {
      document.querySelector('.login-message').textContent =
        'Wrong username or password!';
    }
  };
  xhr.send(JSON.stringify(loginData));
};

loadData();

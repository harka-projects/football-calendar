import { checkCredentials } from './users.js'
import http from 'http'
import fs from 'fs'

const dataPath = '/home/daniel/code/football-calendar/backend'

const server = http
  .createServer((req, res) => {
    res
      .setHeader(
        'Access-Control-Allow-Origin',
        'https://football-calendar.harka.com'
      )
      .setHeader('Access-Control-Allow-Credentials', 'true')
      .setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS')
      .setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
      )

    if (req.url === '/football-calendar/load') {
      const data = fs.readFileSync(`${dataPath}/data.json`, 'utf8')
      res.setHeader('Content-Type', 'application/json').writeHead(200).end(data)
    } else if (req.method === 'OPTIONS') {
      res.writeHead(200).end()
    } else if (req.url === '/football-calendar/save') {
      let body = ''
      req.on('data', chunk => {
        body += chunk.toString()
      })
      req.on('end', () => {
        fs.writeFileSync(`${dataPath}/data.json`, body)
        res.writeHead(200).end()
      })
    } else if (req.url === '/football-calendar/login') {
      let body = ''
      req.on('data', chunk => {
        body += chunk.toString()
      })
      req.on('end', () => {
        const loginData = JSON.parse(body)

        if (checkCredentials(loginData.username, loginData.password)) {
          res.writeHead(200).end()
        } else {
          res.writeHead(401).end()
        }
      })
    } else if (req.url === '/football-calendar/reset') {
      res.setHeader('Content-Type', 'application/json').writeHead(200)
      const sportData = fs.readFileSync(`${dataPath}/sportData.json`, 'utf8')
      res.end(sportData)
      fs.writeFileSync(
        `${dataPath}/data.json`,
        fs.readFileSync(`${dataPath}/sportData.json`, 'utf8')
      )
    } else {
      res
        .writeHead(301, {
          Location: `https://football-calendar.harka.com${req.url}`,
        })
        .end()
    }
  })
  .listen(3012, err => {
    if (err) return console.log(err)
  })
